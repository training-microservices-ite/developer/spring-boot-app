/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.training.microservices.service;

import com.training.microservices.entity.db.Department;
import com.training.microservices.entity.db.Employee;
import com.training.microservices.entity.nodb.EmployeeRequest;
import com.training.microservices.repository.RepoDepartment;
import com.training.microservices.repository.RepoEmployee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cache.CacheType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Fernando
 */
@Service
@Transactional
public class EmployeeService{
    
    @Autowired
    private RepoEmployee repo;
    
    @Autowired
    private RepoDepartment repoDept;
    
    public Employee createEmployee(EmployeeRequest employee) throws Exception{
        if(repo.countByEmailOrPhone(employee.getEmail(), employee.getPhone()) > 0){
            throw new Exception("Duplicate entry on email or phone");
        }
        Department dept = new Department();
        Employee employee2 = new Employee();
        employee2.setName(employee.getName());
        employee2.setDepartment(dept);
        employee2.setPhone(employee.getPhone());
        employee2.setEmail(employee.getEmail());
        employee2.setDepartment(repoDept.findById(employee.getDepartmentId()).orElse(null));
        
        return repo.save(employee2);
        
    }
    
    public Iterable<Employee> findAll(){
        return repo.findAll();
    }
    
    
    
}
