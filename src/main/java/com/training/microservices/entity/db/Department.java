/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.training.microservices.entity.db;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Fernando
 */
@Entity
@Table(name="tbl_department")
@NoArgsConstructor // Anotasi konstruktor kosong
@AllArgsConstructor // Anotasi konstruktor dengan full parameter
@Data // Auto getter setter
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;
    
    @Column(name="department_name")
    protected String name;
    
    @Column(name="department_address")
    protected String address;
    
    @Column(name="department_code", nullable = false)
    protected String code;
}
