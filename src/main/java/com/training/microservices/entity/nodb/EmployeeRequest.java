/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.training.microservices.entity.nodb;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Fernando
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeRequest {
    @NotEmpty(message = "Name is required")
    protected String name;
    
    @NotEmpty(message = "Email is required")
    @Email(message = "Email format is invalid")
    protected String email;
    
    @NotEmpty(message = "Phone is required")
    @Pattern(regexp = "[0-9]{8,13}", message = "Phone format is wrong") // Validasi angka dengan panjang 8-13
    protected String phone;
    
    @NotNull(message = "Department is required")
    protected Long departmentId;
}
