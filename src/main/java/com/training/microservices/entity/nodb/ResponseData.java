/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.training.microservices.entity.nodb;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Fernando
 * @param <T>
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseData<T> {
    protected boolean status;
    protected ArrayList<String> messages = new ArrayList<>();
    protected T data;
}   
