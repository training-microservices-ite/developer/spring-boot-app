/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.training.microservices.repository;

import com.training.microservices.entity.db.Department;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Fernando
 */
@Repository
public interface RepoDepartment extends CrudRepository<Department, Long> {
    public List<Department> findAllByName(String name);
    public List<Department> findAllByNameContaining(String s);
    
    @Query("SELECT d FROM Department d WHERE d.name=:name")
    public List<Department> cariBerdasarkanNama(String name);
}
