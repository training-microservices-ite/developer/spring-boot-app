/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.training.microservices.repository;

import com.training.microservices.entity.db.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Fernando
 */
public interface RepoEmployee extends JpaRepository<Employee, Long>{
    public Long countByEmailOrPhone(String email, String phone);
}
