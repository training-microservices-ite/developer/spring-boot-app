/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.training.microservices.repository;

import com.training.microservices.entity.db.Department;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author Fernando
 */
public interface RepoDepartmentPaging extends PagingAndSortingRepository<Department, Long> {
    
}
