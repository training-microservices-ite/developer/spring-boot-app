/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.training.microservices.res;

import com.training.microservices.entity.db.Department;
import com.training.microservices.entity.nodb.SearchRequest;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.training.microservices.repository.RepoDepartment;
import com.training.microservices.repository.RepoDepartmentPaging;
import java.util.ArrayList;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Fernando
 */
@RestController
@RequestMapping("/api/department")
public class DepartmentController {
    
    @Autowired // Anotasi untuk auto init repo
    protected RepoDepartment repo;
    
    @Autowired
    protected RepoDepartmentPaging repoPaging;
    
    @GetMapping
    public Iterable<Department> get(@RequestParam("id") Long id){ // /api/department?id=xxx
        if(id != null){
            ArrayList rtn = new ArrayList<Department>();
            Department dep = repo.findById(id).orElse(null);
            if(dep != null)
                rtn.add(dep);
            return rtn;
        }
        return repo.findAll();
    }
    
    @GetMapping("{id}")
    public Department FindById(@PathVariable(name = "id") Long id){
        return repo.findById(id).orElse(null);
    }
    
    @PostMapping
    public Department create(@RequestBody Department department){
        return repo.save(department);
    }
    
    @GetMapping("page/{page}")
    public Iterable<Department> getByPage(@PathVariable(name="page") int page){
        int pageSize = 5;
        Pageable pageable = PageRequest.of(page, pageSize);
        return repoPaging.findAll(pageable);
    }
    
    @GetMapping("findName")
    public Iterable<Department> findName(@RequestParam("s") String s){
        return repo.findAllByNameContaining(s);
    }
    
    @GetMapping("findName/{s}")
    public Iterable<Department> findNamePath(@PathVariable(name="s") String s){
        return repo.findAllByNameContaining(s);
    }
    
    @GetMapping("findNameObject")
    public Iterable<Department> findNameObject(SearchRequest request){ // ?keyword=...&x=y
        return repo.findAllByNameContaining(request.getKeyword());
    }
    
    @PostMapping("findNameObject")
    public Iterable<Department> findNameObjectPost(@RequestBody SearchRequest request){
        return repo.findAllByName(request.getKeyword());
    }
    
    @GetMapping("cariBerdasarkanNama")
    public Iterable<Department> cariBerdasarkanNama(@RequestParam("name") String name){
        return repo.cariBerdasarkanNama(name);
    }
    
}
