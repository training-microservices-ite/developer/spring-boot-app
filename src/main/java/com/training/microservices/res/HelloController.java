/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.training.microservices.res;

import com.training.microservices.entity.nodb.Product;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Fernando
 */
@RestController
@RequestMapping("/api/hello")
public class HelloController {
    
    @GetMapping
    public String sayHello(){
        return "Welcome to springboot";
    }
    
    @GetMapping("getZinc")
    public Product getZinc(){
        Product product = new Product("Zinc", 10000);
        return product;
    }
}
