/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.training.microservices.res;

import com.training.microservices.entity.db.Employee;
import com.training.microservices.entity.nodb.EmployeeRequest;
import com.training.microservices.entity.nodb.ResponseData;
import com.training.microservices.repository.RepoEmployee;
import com.training.microservices.service.EmployeeService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Fernando
 */
@RestController
@RequestMapping("api/employee")
public class EmployeeController {
//    @Autowired
//    private RepoEmployee repo;
    @Autowired
    private EmployeeService service;
    
    @GetMapping
    public Iterable<Employee> findAll(){
        return service.findAll();
    }
    
//    @PostMapping("/insert")
//    public Employee insert(@RequestBody Employee employee){
//        return service.createEmployee(employee);
//    }
   
    
    @PostMapping("/insert2")
    public ResponseEntity insert2(@Valid @RequestBody EmployeeRequest employee, Errors error){
        
        ResponseData<Employee> response = new ResponseData<Employee>();
        if(error.hasErrors()){
            for(ObjectError e: error.getAllErrors()){
                response.getMessages().add(e.getDefaultMessage());
            }
            response.setStatus(false);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
        try{
            response.setData(service.createEmployee(employee));
            return ResponseEntity.ok(response);
        }catch(Exception ex){
            response.setStatus(false);
            response.getMessages().add(ex.getMessage());
            return ResponseEntity.badRequest().body(response);
        }
        
        
        

        
        
    }
}
